package exercise.car;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ManualTransmission extends CarTransmission {

	public enum Gear { NEUTRAL, GEAR_1, GEAR_2, GEAR_3, GEAR_4, GEAR_5, REVERSE}     
	
	Gear state = Gear.NEUTRAL;
	
	@Override
	public void shiftUp() {
		switch (this.state) {
		case NEUTRAL: state = Gear.GEAR_1; break;
		case GEAR_1: state = Gear.GEAR_2; break;
		case GEAR_2: state = Gear.GEAR_3; break;
		case GEAR_3: state = Gear.GEAR_4; break;
		case GEAR_4: state = Gear.GEAR_5; break;
		case GEAR_5: throw new IllegalStateException("Already at "+Gear.GEAR_5);
		case REVERSE: state = Gear.GEAR_1; break;
		}
		logger.info(String.format("Shifting gear to %s", this.state));		
	}

	@Override
	public void shiftDown() {
		switch (this.state) {
		case NEUTRAL: throw new IllegalStateException("Already at "+Gear.NEUTRAL);
		case GEAR_1: state = Gear.NEUTRAL; break;
		case GEAR_2: state = Gear.GEAR_1; break;
		case GEAR_3: state = Gear.GEAR_2; break;
		case GEAR_4: state = Gear.GEAR_3; break;
		case GEAR_5: state = Gear.GEAR_4; break;
		case REVERSE: state = Gear.GEAR_1; break;
		}
		logger.info(String.format("Shifting gear to %s", this.state));
	}

	@Override
	public void neutral() {
		this.state = Gear.NEUTRAL;
		logger.info(String.format("Shifting gear to %s", this.state));
	}

	@Override
	public void reverse() {
		this.state = Gear.REVERSE;
		logger.info(String.format("Shifting gear to %s", this.state));
	}


	Logger logger = LoggerFactory.getLogger(this.getClass());
}
