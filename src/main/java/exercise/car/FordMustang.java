package exercise.car;

public class FordMustang extends Car {

	CarEngine engine;
	
	public FordMustang() {
		this.maker = "Ford";
		this.cylinder = 6.0;
	}

}
