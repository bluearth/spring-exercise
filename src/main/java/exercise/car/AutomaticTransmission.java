package exercise.car;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AutomaticTransmission extends CarTransmission {

	enum Gear {
		PARK, REVERSE, NEUTRAL, DRIVE
	}

	Gear state = Gear.NEUTRAL;

	@Override
	public void shiftUp() {
		switch (this.state) {
		case PARK:
		case REVERSE:
		case NEUTRAL:
			this.state = Gear.DRIVE;
			break;
		case DRIVE:
			throw new IllegalStateException("Already at top gear");
		}
		logger.info(String.format("Shifting gear to %s", this.state));
	}

	@Override
	public void shiftDown() {
		switch (this.state) {
		case PARK:
		case REVERSE:
		case NEUTRAL:
			throw new IllegalStateException("Already at top gear");
		case DRIVE:
			this.state = Gear.NEUTRAL;
			break;			
		}
		
		logger.info(String.format("Shifting gear to %s", this.state));
	}

	@Override
	public void neutral() {
		this.state = Gear.NEUTRAL;
		logger.info(String.format("Shifting gear to %s", this.state));
	}

	@Override
	public void reverse() {
		this.state = Gear.REVERSE;
		logger.info(String.format("Shifting gear to %s", this.state));
	}

	
	Logger logger = LoggerFactory.getLogger(this.getClass());
}
