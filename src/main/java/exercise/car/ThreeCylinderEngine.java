package exercise.car;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ThreeCylinderEngine extends CarEngine {

	@Override
	public void start() {
		logger.info("3-Cyl Engine starting...");
		super.start();
	};
	
	@Override
	void throttle() {
		logger.info("3-Cyl engine throttling...bffffrrm!");
	}
	
	@Override
	public void stop() {
		logger.info("3-Cyl Engine stopping...");
		super.stop();		
	};	
	
	Logger logger = LoggerFactory.getLogger(this.getClass());

}
