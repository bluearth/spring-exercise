package exercise.car;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CarFactoryApp {

	public static void main(String... args) throws Exception {
		ApplicationContext context = new ClassPathXmlApplicationContext("application-context.xml"); 

		CarFactory factory = context.getBean(CarFactory.class);
		
		Car car = factory.createAutomaticCar();
		takeForAJoyRide(car);
		
		car = factory.createManualCar();
		takeForAJoyRide(car);

		
		((AbstractApplicationContext)context).close();
	}
	
	static void takeForAJoyRide(Car car){
		car.start();
		
		while(true){
			try {
				car.getEngine().getTransmission().shiftUp();
				car.getEngine().throttle();
			}catch(Exception e){
				break;
			}
		}
		// when all is well, park and turn off the engine
		car.getEngine().getTransmission().neutral();
		car.stop();		
	}

}
