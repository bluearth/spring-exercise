package exercise.car;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class V8Engine extends CarEngine {

	@Override
	public void start() {
		logger.info("V8 Engine starting...");
		super.start();
	};
	
	@Override
	public void throttle() {
		logger.info("Vroom vrommm!!!");
	}
	
	@Override
	public void stop() {
		logger.info("V8 Engine stopping...");
		super.stop();		
	};
	
	
	Logger logger = LoggerFactory.getLogger(this.getClass());

}
