package exercise.car;

public abstract class Car {
	
	CarEngine engine;
	
	String maker;
	
	double cylinder;
	
	public void start() {
		if (engine == null)
			throw new IllegalStateException("The car has no engine!");
		
		this.engine.start();
	}

	public void stop() {
		if (engine == null)
			throw new IllegalStateException("The car has no engine!");
		
		this.engine.stop();
	}

	public void setEngine(CarEngine engine) {
		this.engine = engine;
	}

	public CarEngine getEngine() {
		return this.engine;
	}
}
