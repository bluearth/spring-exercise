package exercise.car;

public abstract class CarTransmission {
	
	
	
	abstract void shiftUp();
	abstract void shiftDown();
	abstract void neutral();
	abstract void reverse();
}
