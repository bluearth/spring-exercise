package exercise.car;

public abstract class CarEngine {
	
	enum EngineState { STOPPED, RUNNING }
	
	CarTransmission transmission;
	EngineState state = EngineState.STOPPED;
	
	public void start() {
		if (transmission == null)
			throw new IllegalStateException("The car has no transmission");
	
		switch (this.state) {
		case STOPPED:
			state = EngineState.RUNNING;
			break;
		case RUNNING:
			throw new IllegalStateException("The car is already running");
		}	
	}

	public void stop() {
		switch (this.state) {
		case STOPPED:
			throw new IllegalStateException("The car is already stopped");
		case RUNNING:
			state = EngineState.STOPPED;
			break;
		}
	}

	public void setTransmission(CarTransmission transmission) {
		this.transmission = transmission;
	}
	
	public CarTransmission getTransmission() {
		return this.transmission;
	}
	
	abstract void throttle();
	


}
