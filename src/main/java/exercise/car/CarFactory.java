package exercise.car;

public class CarFactory {
	
	Car createFordMustang(){
		CarTransmission transmission = new ManualTransmission();
		CarEngine engine = new V8Engine();
		engine.setTransmission(transmission);
		Car car = new FordMustang();
		car.setEngine(engine);
		
		return car;
	}
	
	Car createAutomaticCar(Class<CarEngine> engineClass) throws Exception {		
		try {
			CarEngine engine = (CarEngine) engineClass.newInstance();
			CarTransmission transmission = new AutomaticTransmission();
			engine.setTransmission(transmission);
			Car car = new FordMustang();
			car.setEngine(engine);
			
			return car;			
			
		} catch (InstantiationException | IllegalAccessException e) {
			throw new Exception("Failed creating engine for this car", e);
		} 
	}
	
	Class<? extends CarEngine> defaultEngineType;
	
	Car createAutomaticCar() throws Exception {
		try {
			CarEngine engine = this.defaultEngineType.newInstance();
			CarTransmission transmission = new AutomaticTransmission();
			engine.setTransmission(transmission);
			Car car = new Car() {
				String maker = "OEM";
			};
			car.setEngine(engine);
			
			return car;			
			
		} catch (InstantiationException | IllegalAccessException e) {
			throw new Exception("Failed creating engine for this car", e);
		}		
	}

	Car createManualCar() throws Exception {
		try {
			CarEngine engine = this.defaultEngineType.newInstance();
			CarTransmission transmission = new ManualTransmission();
			engine.setTransmission(transmission);
			Car car = new Car() {
				String maker = "OEM";
			};
			car.setEngine(engine);
			
			return car;			
			
		} catch (InstantiationException | IllegalAccessException e) {
			throw new Exception("Failed creating engine for this car", e);
		}		
	}	
	
	public static CarFactory createInstance(){
		CarFactory newInstance = new CarFactory();
		newInstance.defaultEngineType = ThreeCylinderEngine.class;
		return newInstance;
	}
	
}
